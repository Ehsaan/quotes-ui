import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import fetch from 'isomorphic-unfetch'

const styles = theme => ( {
	paper: {
		padding: theme.spacing.unit * 1.5,
		marginBottom: theme.spacing.unit * 1.5,
	},
	text: {
		whiteSpace: 'pre-line' // Break the line on newline sequence (\n)
	}
} )

class Quotation extends React.Component {
	constructor( props ) {
		super( props )
		
		this.state = { item: {
			text:		props.title,
			text:		props.text,
			author:		props.author,
			imdb_data:		{
				title:		'',
				poster:		<Avatar>L</Avatar>,
				creator:	'N/A'
			}
		} }

		var { item } = props

		fetch( 'http://localhost:8080/api/imdb/get/' + item.imdb ).then( async function( result ) {
			const data = await result.json()

			item.imdb_data = {
				title:			data.Title,
				poster:			<Avatar alt={data.Title} src={data.Poster} />,
				creator:		data.Writer
			}

			this.setState( { item } )
		}.bind( this ) ) 
	}

	render() {
		const { classes } = this.props
		const { item } = this.state
		return (
			<Grid item lg={7} xl={6} sm={8} xs={11} key={item.key}>
				<Paper className={classes.paper} elevation={1}>
					<Grid container spacing={16} direction="row" alignItems="center">
						<Grid item>
							{item.imdb_data.poster}
						</Grid>
						<Grid item xs>
							<Typography variant="subheading" noWrap>{item.imdb_data.title}</Typography>
							<Typography variant="caption" noWrap>{item.imdb_data.creator}</Typography>
						</Grid>
					</Grid>
					<Grid container spacing={16} direction="row" alignItems="center">
						<Grid item xs>
							<Typography className={classes.text}>
								{item.text}
							</Typography>
							<Typography align="right" variant="caption">
								<em>posted by {item.author}</em>
							</Typography>
						</Grid>
					</Grid>
				</Paper>
			</Grid>
		)
	}
}

Quotation.propTypes = {
	classes: PropTypes.object.isRequired,
}

export default withStyles( styles )( Quotation )